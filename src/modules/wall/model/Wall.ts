
import { v4 as uuidV4 } from "uuid"

class Wall {
    id?: string;
    totalLitros: number;
    totalLatas: string;
    createdAt: Date;
    constructor() {
        if (!this.id) {
            this.id = uuidV4();
        }
    }
}

export { Wall }