import { Wall } from "../../model/Wall";
import { IWallsRepository, ICalculateWallDTO } from "../IWallsRepository";

interface IReturnCalculateResult {
    totalLitros: number;
    totalLatas: string;
    createdAt: Date;
}

class WallRepository implements IWallsRepository {
    private wallsList: Wall[];
    private static INSTANCE: WallRepository;

    private constructor() {
        this.wallsList = [];
    }

    public static getInstance(): WallRepository {
        if (!WallRepository.INSTANCE) {
            WallRepository.INSTANCE = new WallRepository();
        }
        return WallRepository.INSTANCE;
    }

    calculate({ walls }: ICalculateWallDTO): IReturnCalculateResult {

        const resultLitros = Math.round(this.calculateAreToPaint(this.calculateSizeWalls(walls)))
        
        const cansResult =  {
            totalLitros: resultLitros,
            totalLatas: this.calculateCans(resultLitros),
            createdAt: new Date()
        }

        this.wallsList.push(cansResult)

        delete cansResult.createdAt

        return cansResult
    }

    private calculateSizeWalls(walls): Array<Object> {
        const newArrayWallsWithSizeWalls = [];

        walls.forEach((wall) => {

            const valueTotalWall: number = wall.height * wall.width;

            const objectTemporytoInsertInArray: any = {}

            objectTemporytoInsertInArray['totalSizeWall'] = valueTotalWall

            let totalDoor = 0;
            if (wall.door > 0) {
                totalDoor = 0.80 * 1.90;
                objectTemporytoInsertInArray['totalValueDoor'] = totalDoor * wall.door;
            }
            let totalWindow = 0;
            if (wall.window > 0 ) {
                totalWindow = 2.0 * 1.20;
                objectTemporytoInsertInArray['totalValueWindow'] = totalWindow * wall.window;
            } 

            objectTemporytoInsertInArray

            newArrayWallsWithSizeWalls.push(objectTemporytoInsertInArray)
        })
        return newArrayWallsWithSizeWalls
    }

    private calculateAreToPaint(walls): number {

        let totalInk = 0;
        let aux = 0;
        const LITERS_PER_INK = 5;

        walls.forEach((wall: any) => {

            if (wall.totalValueDoor) {
                if (wall.totalValueWindow) {
                    aux = wall.totalSizeWall - (wall.totalValueDoor + wall.totalValueWindow)
                } else {
                    aux = wall.totalSizeWall - wall.totalValueDoor

                }
            } else {
                aux = wall.totalSizeWall
            }

            totalInk = totalInk + aux
        })

        return totalInk / LITERS_PER_INK
    }

    private calculateCans(totalPaint): string {

        var cans = [18, 3.6, 2.5, 0.5];
        var text = '';

        for (var x = 0; x < cans.length; x++) {
          
            if (totalPaint >= cans[x]) {

                var div = Math.floor(totalPaint / cans[x]);

                text += div + " lata de " + cans[x] + ' ';

                totalPaint -= div * cans[x];
            }

            if(totalPaint < 0.5 && totalPaint > 0){

                totalPaint =  totalPaint.toFixed(2) - 0.5   ;
             
                text += " 1" + " lata de " + 0.5 + " Vai sobrar: " + Math.abs(totalPaint.toFixed(2)) + ' de Tinta';
            }
        }

        return text;
    }

    list(): Wall[] {
        return this.wallsList
    }
}

export { WallRepository }