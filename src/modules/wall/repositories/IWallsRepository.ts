import { Wall } from "../model/Wall";

interface ICalculateWallDTO {
    walls: [
         {
            door: number,
            window: number,
            height: number;
            width: number;
        }
    ]
}

interface IReturnCalculateResultDTO {
    totalLitros: number;
    totalLatas: string;
}

interface IWallsRepository {
    list(): Wall[];
    calculate({ walls }: ICalculateWallDTO): IReturnCalculateResultDTO;
}
export { IWallsRepository, ICalculateWallDTO }