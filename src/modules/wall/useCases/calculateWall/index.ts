import { WallRepository } from "../../repositories/implementations/WallsRepository";
import { CalculateWallController } from "./CalculateWallController";
import { CalculateWallUseCase } from "./CalculateWallUseCase";


const wallRepository =  WallRepository.getInstance();
const calculateWallUseCase = new CalculateWallUseCase(wallRepository);
const calculateWallController = new CalculateWallController(calculateWallUseCase);

export {calculateWallController}