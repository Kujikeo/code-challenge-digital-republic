
import { IWallsRepository } from "../../repositories/IWallsRepository";



interface IRequest {
    walls: [
        {
            door: number,
            window: number,
            height: number;
            width: number;
        }
    ]
}

class CalculateWallUseCase {

    constructor(private wallsRepository: IWallsRepository) { }

    execute({ walls }: IRequest) {
      
        walls.forEach((wall) => {
            if(!wall.door && wall.door != 0) {
                throw new Error("In body request missing value door, not be null");
            }
            if(!wall.window && wall.window != 0){
                throw new Error("In body request missing value window, can not be null");
            }
            if(this.validateSizeWall(wall)){
                throw new Error("The wall is bigger than 50m or than less 1m.");
            }
            if(this.validateSizeWallWithDoor(wall)){
                throw new Error("The wall is and Door don have than more 30cm space in height.");
            }
        })

        return this.wallsRepository.calculate({ walls })
    }


   private validateSizeWallWithDoor(wall): boolean {
       
        if(wall.door){
            const temporyResult = wall.height - 1.90;
            if(temporyResult < 0.30){
                return true
            }
        }
        return false
    }

    private validateSizeWall(wall): boolean {
        
        if (wall.height > 50 || wall.height < 1 || wall.width < 1 || wall.width > 50) {
            return true;
        }
        return false;
    }
}

export { CalculateWallUseCase }