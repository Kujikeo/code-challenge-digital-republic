import { Request, Response } from "express";
import { CalculateWallUseCase } from "./CalculateWallUseCase";

class CalculateWallController {

    constructor(private calculateWallUseCase: CalculateWallUseCase) { }

    handle(request: Request, response: Response): Response {

        const { walls } = request.body;

        try {
            return response.status(201).json(this.calculateWallUseCase.execute({ walls }));

        } catch (error) {
 
            return response.status(500).json({ error: error.message });
        }

    }
}
export { CalculateWallController }