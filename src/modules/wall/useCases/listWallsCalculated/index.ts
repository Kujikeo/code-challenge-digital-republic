import { WallRepository } from "../../repositories/implementations/WallsRepository";
import { CalculateWallController } from "./ListWallsCalculatedController";
import { ListWallsCalculatedUseCase } from "./ListWallsCalculatedUseCase";


const wallRepository =  WallRepository.getInstance();
const listCalculateWallUseCase = new ListWallsCalculatedUseCase(wallRepository);
const listWallsCalculateController = new CalculateWallController(listCalculateWallUseCase);

export {listWallsCalculateController}