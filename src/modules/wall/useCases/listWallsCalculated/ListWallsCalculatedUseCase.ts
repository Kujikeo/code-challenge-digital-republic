
import { Wall } from "../../model/Wall";
import {IWallsRepository } from "../../repositories/IWallsRepository";



class ListWallsCalculatedUseCase {
    
    constructor(private wallsRepository: IWallsRepository) {}

    execute():Wall[] {
        const walls = this.wallsRepository.list();
        return walls;
    }

}

export { ListWallsCalculatedUseCase }