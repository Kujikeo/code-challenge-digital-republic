import { Request, Response } from "express";
import { ListWallsCalculatedUseCase } from "./ListWallsCalculatedUseCase";

class CalculateWallController {

       
    constructor(private listWallsCalculatedUseCase: ListWallsCalculatedUseCase) {}


    handle(request: Request, response: Response): Response {

        return response.status(200).json(this.listWallsCalculatedUseCase.execute()); 

    }
}
export { CalculateWallController }