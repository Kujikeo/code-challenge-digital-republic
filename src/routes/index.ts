import { Router } from "express";
import { calculateRoutes } from "./walls.routes";


const router = Router();

router.use("/calculate",calculateRoutes);

export {router}
