import { Request, Response, Router } from "express";
import { calculateWallController } from "../modules/wall/useCases/calculateWall";
import { listWallsCalculateController } from "../modules/wall/useCases/listWallsCalculated";


const calculateRoutes = Router();

calculateRoutes.post("/", (request: Request, response: Response) => {
        return calculateWallController.handle(request, response);
})

calculateRoutes.get("/", (request: Request, response: Response) => {
        return listWallsCalculateController.handle(request, response);
})

export { calculateRoutes }